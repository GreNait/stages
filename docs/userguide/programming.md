## Building the queue

First, build a queue with the builder. Provide a json (e.g. `StagesCLI`) to the builder.

```python
json = ...

from stages.stage_queue_builder import StageQueueBuilder
queue = StageQueueBuilder().parse_json(json)
```

If you have the queue, you can start it:

```python
queue.start()
```

However, to still show the status of the queue, stages and timer, it might be useful to start it as a separate thread.

```python
import threading

x = threading.Thread(target=queue.start, args(), daemon=True)
x.start()
```

Timer and stages will publish their status to the queue. Which can output that status as well.

```python
print(queue.status)
#x.join() <- If you want to wait until the thread is finished
```
