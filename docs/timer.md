# UML overview timer

```mermaid
classDiagram
    direction RL
    class Timer{
        int hours
        int minutes
        int seconds
        int remaining_seconds
        list observers
        attach_observers() void
        start() void
        update() void
    }

    class InterfaceSubject{
        <<Interface>>
        start()*
        update()*
    }

    Timer ..|> InterfaceSubject  : implements

```

## Class overview

::: stages.timer
