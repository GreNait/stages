## Using the stage

```python
from stages.stage import Stage
stage = Stage("test stage")
```

### You can also directly attach a timer

```python
from stages.timer import Timer
stage = Stage("test stage").attach_timer(Timer(0,0,30))
```

## UML overview stage

```mermaid
classDiagram
    direction RL
    class Stage{
        String name
        String status
        create_timer() void
        start_subject() void
        update() void
    }

    class InterfaceObserver{
        <<Interface>>
        update()*
    }

    Stage ..|> InterfaceObserver
```

## Overview class

::: stages.stage
