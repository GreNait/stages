# Stages

This package provides modules for generating stages with timers and execute them.

Please see the [Userguide](userguide.md) for more information.
