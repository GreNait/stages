# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.1.1a1 (2022-04-08)

### Feat

- **stage_queue,-stage**: update from stage to queue
- **timer.py,-stage.py**: added a stop to timer

### Fix

- **pyproject.toml**: added versioning from commitizen bump

## 0.1.0 (2022-04-08)

### Fix

- **setup.cfg**: increase the version to -dev1

### Feat

- **stages/***: added logging for debug and critical information
- **stage_queue_builder,-docs/*,-unit_tests/test_stage_queue_builder.py**: added a builder object to create a stage queue
- **stage_queue.py**: added a queue to chain stages
- **stage,-timer,-interface_observer,-interface_subject**: Added a stage (observer) and changed timer to a subject (publisher)
- **timer.py**: add first draft of the timer module and unit tests for it
- **ALL**: initial draft of the project

### Refactor

- **.pre-commit,-requirements_dev.txt**: added black, pytest, etc. to requirements
- **test_stage_queue_builder.py**: refactored the unit tests
- **stages/*,-docs/*,-tests/***: replaced object creation with dependency injections
