import pytest
from unittest.mock import patch

from stages.stage import Stage
from stages.stage import Timer
from stages.stage_queue import StageQueue


@pytest.fixture
def test_stage():
    stage = Stage("Test").attach_timer(Timer(0, 0, 1))
    return stage


@pytest.fixture
def test_queue(test_stage):
    stage_queue = StageQueue("Test stage queue")
    stage_queue.attach_stage(test_stage)
    return stage_queue


def test_attach_stage(test_stage):
    stage_queue = StageQueue(name="Test stage queue")
    stage_queue.attach_stage(test_stage)

    assert stage_queue.queue[0].name == "Test"

    stage_queue.attach_stage(
        Stage("Test 2").attach_timer(Timer(0, 0, 1)).attach_timer(Timer(0, 0, 2))
    )

    assert stage_queue.queue[-1].name == "Test 2"
    assert stage_queue.queue[-1].subjects[0].seconds == 1
    assert stage_queue.queue[-1].subjects[1].seconds == 2


@patch("time.sleep", return_value=None)
def test_run_queue(patched_time_sleep, test_queue):
    assert test_queue.queue[0].status == None
    test_queue.start()
    assert test_queue.queue[0].status != None
