import pytest

from unittest.mock import patch

from stages.timer import Timer
from stages.stage import Stage
from stages.stage_queue import StageQueue


@pytest.fixture
def test_stage():
    return Stage(name="Test timer")


def test_define_stage():
    stage = Stage(name="Timer")

    assert stage.name == "Timer"


def test_change_name(test_stage):
    assert test_stage.name == "Test timer"
    test_stage.name = "Different timer"
    assert test_stage.name == "Different timer"


def test_attach_timer_and_add_to_subjects(test_stage):
    test_stage.attach_timer(Timer(0, 0, 1)).attach_timer(Timer(0, 0, 1))
    assert type(test_stage.subjects[0]) == Timer
    assert len(test_stage.subjects) == 2


@patch("time.sleep", return_value=None)
def test_start_subject(patched_time_sleep, test_stage):
    test_stage.attach_timer(Timer(0, 0, 1))
    test_stage.start()
    assert test_stage.subjects[-1].remaining_seconds == 0


@patch("time.sleep", return_value=None)
def test_check_status(patched_time_sleep, test_stage):
    test_stage.attach_timer(Timer(0, 0, 1))
    assert test_stage.status == None

    with pytest.raises(Exception):
        test_stage.status = True

    test_stage.start()
    assert test_stage.status != None


def test_attach_observer(test_stage):
    test_stage.attach_timer(Timer(0, 0, 0))

    test_queue = StageQueue(name="Test queue")
    test_queue.attach_stage(test_stage)

    test_stage.start()
    assert test_stage.status == f"{test_stage.status}"
    assert test_queue.status == f"{test_stage.name} - {test_stage.status}"
