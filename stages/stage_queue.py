from __future__ import annotations

from typing import List

import logging

logger = logging.getLogger(__name__)


class StageQueue:
    """A stage queue contain(s) a stage(s).

    Args:
        name (str): The name of the queue

    Attributes:
        queue (list): A list containing all stages
    """

    def __init__(self, name: str) -> None:
        self._name = name
        self._queue: List = []
        self._status = None

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self) -> None:
        logger.critical("Cannot change name")
        raise Exception("Cannot change name")

    @property
    def queue(self) -> List[Stage]:
        return self._queue

    @queue.setter
    def queue(self) -> None:
        logger.critical("Cannot set stage queue")
        raise Exception("Cannot set stage queue")

    @property
    def status(self) -> str:
        return self._status

    @status.setter
    def status(self) -> None:
        logger.critical("Cannot set status")
        raise Exception("Cannot set status")

    def attach_stage(self, stage) -> StageQueue:
        """Attach a stage to the queue

        Args:
            stage (Stage): The define stage object

        Returns:
            self (StageQueue): Returns the own reference
        """
        logger.debug(f"Append stage (publisher) and add queue as observer")
        stage.attach_queue(self)
        self._queue.append(stage)
        return self

    def start(self) -> None:
        """Start all subjects within the queue"""
        for stage in self._queue:
            logger.debug(f"Start subject {stage}")
            stage.start()

    def update(self, value: any) -> None:
        """Get update from running stage (publisher)"""
        logger.debug(f"Got {value} from subject (stage)")
        self._status = value
