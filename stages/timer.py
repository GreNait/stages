import time
import uuid

from typing import Any, List
from stages.interface_observer import InterfaceObserver
from stages.interface_subject import InterfaceSubject

import logging

logger = logging.getLogger(__name__)


class Timer(InterfaceSubject):
    """
    Timer object to start a countdown and update status.

    Args:
        hours (int): Hours
        minutes (int): minutes
        seconds (int): seconds

    Attributes:
        remaining_seconds (int): How many seconds are left.
        observers (list): What observers are registered.
    """

    def __init__(self, hours: int, minutes: int, seconds: int):

        self._id = uuid.uuid4()
        self._last_update = None

        self._hours = hours
        self._minutes = minutes
        self._seconds = seconds

        self._observers: List = []

        self._total_seconds()

        self.__stop = False

    def _total_seconds(self):
        self._remaining_seconds = (
            self._hours * 3600 + self._minutes * 60 + self._seconds
        )
        logger.debug("total_seconds", self._remaining_seconds)

    def __raise_setter_exception(self, variable: str) -> None:
        logger.critical(f"called setter on {variable}")
        raise Exception(f"Cannot change {variable} after initialising Timer")

    @property
    def hours(self):
        return self._hours

    @hours.setter
    def hours(self, value: int):
        self.__raise_setter_exception(variable="hours")

    @property
    def minutes(self):
        return self._minutes

    @minutes.setter
    def minutes(self, value: int):
        self.__raise_setter_exception(variable="minutes")

    @property
    def seconds(self):
        return self._seconds

    @seconds.setter
    def seconds(self, value: int):
        self.__raise_setter_exception(variable="seconds")

    @property
    def remaining_seconds(self) -> int:
        return self._remaining_seconds

    @remaining_seconds.setter
    def remaining_seconds(self) -> None:
        self.__raise_setter_exception(variable="remaining seconds")

    @property
    def observers(self) -> List:
        return self._observers

    @observers.setter
    def observers(self) -> None:
        self.__raise_setter_exception(variable="observers")

    @property
    def id(self) -> str:
        return self._id

    @id.setter
    def id(self) -> None:
        self.__raise_setter_exception("Cannot change id")

    @property
    def last_update(self) -> str:
        return self._last_update

    @last_update.setter
    def last_update(self) -> None:
        self.__raise_setter_exception("Cannot change the last update")

    def attach_observer(self, observer: InterfaceObserver) -> None:
        """Add an observer object (listener)"""
        logger.debug(f"Attached observer {observer}")
        self._observers.append(observer)

    def start(self) -> None:
        """Starts the countdown"""
        logger.debug("Start of timer")
        self.__stop = False
        self.update(self._remaining_seconds)

        if self._total_seconds == 0:
            self._total_seconds()

        while self._remaining_seconds > 0 and self.__stop == False:
            time.sleep(0.1)
            self._remaining_seconds -= 0.1
            self._remaining_seconds = round(self._remaining_seconds, 2)
            self.update(self._remaining_seconds)

        if self._remaining_seconds < 0:
            self._remaining_seconds = 0

    def update(self, value: Any) -> None:
        """Update all observers with the remaining time

        Args:
            value (any): update the observer with value
        """
        logger.debug(f"Update observer with value {value}")
        self._last_update = f"{self._id} - {value}"
        for observer in self._observers:
            observer.update(self._last_update)

    def stop(self) -> None:
        """Stop the timer, can be restarted with start()"""
        self.__stop = True
