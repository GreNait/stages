from __future__ import annotations
from stages.interface_subject import InterfaceSubject
from stages.stage_queue import StageQueue

from stages.timer import Timer
from stages.interface_observer import InterfaceObserver

from typing import Any, List

import logging

logger = logging.getLogger(__name__)


class Stage(InterfaceObserver, InterfaceSubject):
    """
    A stage contains and starts an subject.

    It also provides information about the subject status.
    This status is obtained by subscribing to the subject

    Args:
        name (str): The name of the stage

    Attributes:
        status (str): Status of the stage
    """

    def __init__(self, name: str) -> None:
        self._name = name
        self._subjects: List = []
        self._status: Any = None
        self._queue: Any = None

    @property
    def status(self) -> Any:
        return self._status

    @status.setter
    def status(self) -> None:
        logger.critical("Cannot change status")
        raise Exception("Cannot change status")

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, name: str) -> None:
        self._name = name

    @property
    def subjects(self) -> List:
        return self._subjects

    @subjects.setter
    def subjects(self) -> None:
        logger.critical("Cannot set subject")
        raise Exception("Cannot set subject")

    def _add_subject(self, subject) -> None:
        logger.debug(f"Added subject {subject}")
        self._subjects.append(subject)

    def attach_timer(self, timer: Timer) -> Stage:
        """Create a timer object and attach itself as an observer

        Args:
            timer (Timer): attach timer to subjects

        Returns:
            self (Stage): A stage object used for calling create_timer
        """
        logger.debug("Attach stage to timer")
        timer.attach_observer(self)
        self._add_subject(timer)
        return self

    def attach_queue(self, queue: StageQueue) -> None:
        """Attach stage queue (observer)"""
        logger.debug("Attach queue to stage")
        self._queue = queue

    def start(self) -> None:
        """ "Start all subjects (they should have implemented InterfaceSubject)"""

        for subject in self._subjects:
            logger.debug(f"Start subject {subject}")
            subject.start()

    def update(self, value: Any) -> None:
        """Update the status of the stage (via the subject)"""
        logger.debug(f"Update from publisher {value}")
        self._status = value

        if self._queue != None:
            value = f"{self.name} - {self._status}"
            logger.debug(f"Update queue(observer) with {value}")
            self._queue.update(value)
