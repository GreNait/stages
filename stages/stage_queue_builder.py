import json

from stages.timer import Timer
from stages.stage import Stage
from stages.stage_queue import StageQueue

import logging

logger = logging.getLogger(__name__)


class StageQueueBuilder:
    """Using a json string to build a stage queue"""

    def _create_timer(self, timer_config: dict) -> Timer:
        logger.debug(f"Return timer")
        return Timer(
            hours=timer_config["hours"],
            minutes=timer_config["minutes"],
            seconds=timer_config["seconds"],
        )

    def _parse_subjects(self, stage_config: dict, stage: Stage) -> Stage:
        for subject in stage_config["subjects"]:
            if subject["type"] == "timer":
                stage.attach_timer(self._create_timer(subject))

        logger.debug("Return stage")
        return stage

    def _create_stage(self, stage_config: dict) -> Stage:
        logger.debug("Create stage")
        stage = Stage(name=stage_config["name"])

        return self._parse_subjects(stage_config, stage)

    def _create_stage_queue(self, stage_queue_config: dict) -> StageQueue:
        logger.debug("Create stage queue")
        stage_queue = StageQueue(name=stage_queue_config["name"])

        for stage in stage_queue_config["stages"]:
            stage_queue.attach_stage(self._create_stage(stage))

        logger.debug("Return stage queue")
        return stage_queue

    def parse_json(self, config: str) -> StageQueue:
        """Convert a string to a json and build the stage queue
        Args:
            config (str): The stage queue config as json string. Will be converted to a dict.

        Returns:
            StageQueue: A fully configured stage queue.

        """
        logger.debug(f"Recevied json to parse: {config}")
        config = json.loads(config)

        return self._create_stage_queue(config)
